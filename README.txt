== Description ==

This module display a list of your public beevou electronic digital vouchers and coupons, it requires an active beevou.com account that will be used to connect to the beevou.net API with your credentials, also you can specify the data to display, like the voucher icon and description.

== Installation ==

1. Upload the folder `beevou` to the `/sites/all/modules/` directory or upload the zip through drupal administrator panel
2. Activate the module through the 'Modules' menu in Drupal
3. Configure your beevou.com account under the settings menu